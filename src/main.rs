const MAX_INT : u32 = 32768;

fn recursion_test(r0 : u32, r1 : u32, r7 : u32, cache: &mut [[u16; 0x8000]; 5]) -> u32 {
    if cache[r0 as usize][r1 as usize] != 0xffff {
        return cache[r0 as usize][r1 as usize] as u32
    }

    if r0 == 0 {
        return (r1 + 1) & 0x7fff
    }

    if r1 == 0 {
        return recursion_test(r0 - 1, r7, r7, cache)
    }

    let tmp = recursion_test(r0, r1 - 1, r7, cache);
    cache[r0 as usize][r1 as usize - 1] = tmp as u16;
    let r1 = tmp;

    return recursion_test(r0 - 1, r1, r7, cache)
}


fn main() {
    for x in 0..0x8000 {
        if x % (0x8000 / 20) == 0 {
            println!("Progress...{}% Completed", (x as f32 / 0x8000 as f32) * 100 as f32);
        }

        let mut cache = [[0xffffu16; 0x8000]; 5];

        let (r0, r1, r7) = (4, 1, x);
        let val = recursion_test(r0, r1, r7, &mut cache);


        if val == 6 {
            println!("Found the magic value : {}", r7);
            break;
        }
    }

}
